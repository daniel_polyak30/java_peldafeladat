Alkalmazás indítása parancssorból:

1. git clone https://daniel_polyak30@bitbucket.org/daniel_polyak30/java_peldafeladat.git
2. cd .\java_peldafeladat\ 
3. mvn package
4. cd .\target\
5. java -jar java_peldafeladat-0.0.1-SNAPSHOT.jar fully.qualified.package.Application

A program Postman-en keresztül tesztelhető az alábbi API végponton keresztül: http://localhost:8081/szemely/