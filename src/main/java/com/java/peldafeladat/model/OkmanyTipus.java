package com.java.peldafeladat.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OkmanyTipus implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String kod;
	private String ertek;
	
	public String getKod() {
		return kod;
	}
	
	public void setKod(String kod) {
		this.kod = kod;
	}
	
	public String getErtek() {
		return ertek;
	}
	
	public void setErtek(String ertek) {
		this.ertek = ertek;
	}
}
