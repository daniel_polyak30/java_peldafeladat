package com.java.peldafeladat.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.java.peldafeladat.dto.OkmanyDTO;
import com.java.peldafeladat.dto.SzemelyDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorListWithOkmanyWrapper implements Serializable {

	private static final long serialVersionUID = 2L;
	
	private OkmanyDTO okmanyDTO;
	private List<String> errorList;
	
	public ErrorListWithOkmanyWrapper() {
	}
	
	public ErrorListWithOkmanyWrapper(OkmanyDTO okmanyDTO, List<String> errorList) {
		this.okmanyDTO = okmanyDTO;
		this.errorList = errorList;
	}
	
	public OkmanyDTO getOkmanyDTO() {
		return okmanyDTO;
	}
	
	public void setOkmanyDTO(OkmanyDTO okmanyDTO) {
		this.okmanyDTO = okmanyDTO;
	}
	
	public List<String> getErrorList() {
		return errorList;
	}
	
	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
}
