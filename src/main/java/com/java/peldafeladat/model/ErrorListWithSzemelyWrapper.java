package com.java.peldafeladat.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.java.peldafeladat.dto.SzemelyDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorListWithSzemelyWrapper implements Serializable {

	private static final long serialVersionUID = 2L;
	
	private SzemelyDTO szemelyDTO;
	private List<String> errorList;
	
	public ErrorListWithSzemelyWrapper() {
	}
	
	public ErrorListWithSzemelyWrapper(SzemelyDTO szemelyDTO, List<String> errorList) {
		this.szemelyDTO = szemelyDTO;
		this.errorList = errorList;
	}
	
	public SzemelyDTO getSzemelyDTO() {
		return szemelyDTO;
	}
	
	public void setSzemelyDTO(SzemelyDTO szemelyDTO) {
		this.szemelyDTO = szemelyDTO;
	}
	
	public List<String> getErrorList() {
		return errorList;
	}
	
	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
}
