package com.java.peldafeladat.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class Utils {

	public static String convertObjectToJsonString(Object object) {
    	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    	String json = "";
    	try {
			json = ow.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
    	
    	return json;
    }
	
	public static LocalDate convertDateToLocalDate(Date date) {
		return date.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
	}
}
