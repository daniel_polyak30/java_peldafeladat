package com.java.peldafeladat.service;

import java.util.List;

import com.java.peldafeladat.dto.OkmanyDTO;
import com.java.peldafeladat.model.ErrorListWithOkmanyWrapper;

public interface OkmanyService {

	List<ErrorListWithOkmanyWrapper> validateOkmanyDTOList(List<OkmanyDTO> okmanyDTOList);
}
