package com.java.peldafeladat.service;

import com.java.peldafeladat.dto.SzemelyDTO;

public interface SzemelyService {

	String validateSzemely(SzemelyDTO szemelyDTO);
}
