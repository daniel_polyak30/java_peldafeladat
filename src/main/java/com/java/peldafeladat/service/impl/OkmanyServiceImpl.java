package com.java.peldafeladat.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.peldafeladat.dto.OkmanyDTO;
import com.java.peldafeladat.model.ErrorListWithOkmanyWrapper;
import com.java.peldafeladat.model.OkmanyTipus;
import com.java.peldafeladat.service.OkmanyService;
import com.java.peldafeladat.util.Utils;

@Service
public class OkmanyServiceImpl implements OkmanyService {
	private static final String JPEG_FORMAT = "image/jpeg";
	
	private List<OkmanyTipus> okmanyTipusDTOList;
	
	@PostConstruct
	public void loadOkmanyTipusDTOList() {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<OkmanyTipus>> typeReference = new TypeReference<List<OkmanyTipus>>(){};
		try (InputStream inputStream = TypeReference.class.getResourceAsStream("/json/kodszotar46_okmanytipus.json")) {
			okmanyTipusDTOList = mapper.readValue(inputStream, typeReference);
		} catch (IOException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
	}

	public List<ErrorListWithOkmanyWrapper> validateOkmanyDTOList(List<OkmanyDTO> okmanyDTOList) {
		List<ErrorListWithOkmanyWrapper> errorListWithOkmanyWrapperList = new ArrayList<>();
		for (OkmanyDTO okmanyDTO : okmanyDTOList) {
			List<String> errorList = new ArrayList<>();
			// Okmány típus
			if (StringUtils.isBlank(okmanyDTO.getOkmTipus())) {
	    		errorList.add("Okmány típusa mező megadása kötelező!");
	    	} else {
	    		Optional<OkmanyTipus> okmanyTipusOptional = okmanyTipusDTOList.stream().filter(item -> item.getKod().equals(okmanyDTO.getOkmTipus())).findFirst();
	    		if (okmanyTipusOptional.isPresent()) {
	    			OkmanyTipus okmanyTipus = okmanyTipusOptional.get();
	    			// Okmányszám
	    			validateOkmanyszam(okmanyDTO, okmanyTipus, errorList);
	    		} else {
	    			errorList.add("Nem létező okmány típust adott meg!");
	    		}
	    	}
			
			// Okmánykép
			if (okmanyDTO.getOkmanyKep() == null) {
				errorList.add("Okmánykép megadása kötelező!");
			} else {
				validateOkmanykep(okmanyDTO.getOkmanyKep(), errorList);
			}
			
			// Lejárati idő
			if (okmanyDTO.getLejarDat() == null) {
				errorList.add("Lejárati idő megadása kötelező!");
			} else {
				LocalDate lejarDat = Utils.convertDateToLocalDate(okmanyDTO.getLejarDat());
				boolean ervenyes = false;
				if (lejarDat.isAfter(LocalDate.now())) {
					ervenyes = true;
				}
				
				okmanyDTO.setErvenyes(ervenyes);
			}
			
			
			errorListWithOkmanyWrapperList.add(new ErrorListWithOkmanyWrapper(okmanyDTO, errorList));
		}
		
		List<String> errorList = new ArrayList<>();
		Map<String, List<OkmanyDTO>> ervenyesOkmanyDTOMap = okmanyDTOList.stream().filter(item -> item.isErvenyes()).collect(Collectors.groupingBy(OkmanyDTO::getOkmTipus));
		for (Map.Entry<String, List<OkmanyDTO>> entry : ervenyesOkmanyDTOMap.entrySet()) {
			if (entry.getValue().size() > 1) {
				errorList.add(entry.getValue().get(0).getOkmTipus() + " típusú okmányból több, mint egy érvényes van megadva!");
			}
		}
		
		errorListWithOkmanyWrapperList.add(new ErrorListWithOkmanyWrapper(null, errorList));
		
		return errorListWithOkmanyWrapperList;
	}
	
	private void validateOkmanyszam(OkmanyDTO okmanyDTO, OkmanyTipus okmanyTipus, List<String> errorList) {
		String okmanySzam = okmanyDTO.getOkmanySzam();
		switch (okmanyTipus.getKod()) {
			case "1":
				// Személyazonosító igazolvány
	    		if (!Pattern.matches("([0-9]{6})([A-Z]{2})", okmanySzam)) {
	    			errorList.add("A megadott " + okmanyTipus.getErtek() + " (" + okmanySzam + ") típusú okmány csak a következő formátumú lehet: 6 szám + 2 nagy betű");
	    		}
				break;
			case "2":
				// Útlevél
	    		if (!Pattern.matches("([A-Z]{2})([0-9]{7})", okmanySzam)) {
	    			errorList.add("A megadott " + okmanyTipus.getErtek() + " (" + okmanySzam + ") típusú okmány csak a következő formátumú lehet: 2 nagy betű + 7 szám");
	    		}
				break;
			case "3":
				// Vezetői engedély
	    		if (!Pattern.matches("([A-Z]{2})([0-9]{6})", okmanySzam)) {
	    			errorList.add("A megadott " + okmanyTipus.getErtek() + " (" + okmanySzam + ") típusú okmány csak a következő formátumú lehet: 2 nagy betű + 6 szám");
	    		}
				break;
			case "4":
			case "5":
			case "6":
				// Egyéb
				if (okmanySzam.length() > 10) {
					errorList.add("A megadott " + okmanyTipus.getErtek() + " (" + okmanySzam + ") típusú okmány szám nem lehet hosszabb 10 karakternél!");
				}
				break;
		}
	}
	
	private void validateOkmanykep(byte[] imageByteArray, List<String> errorList) {
		try (InputStream inputStream = new ByteArrayInputStream(imageByteArray)) {
			String contentType = URLConnection.guessContentTypeFromStream(inputStream);
			if (!JPEG_FORMAT.equals(contentType)) {
				errorList.add("Nem megfelelő fájlformátumot adott meg!");
			}
			MultipartFile file = new MockMultipartFile("new file name", "Original file name", MediaType.APPLICATION_OCTET_STREAM.toString(), inputStream);
			BufferedImage image = ImageIO.read(file.getInputStream());
			if (image != null) {
				if (image.getWidth() != 827 || image.getHeight() != 1063) {
					errorList.add("Nem megfelelő méretű képet adott meg! (" + image.getWidth() + "x" + image.getHeight() + ")");
				}
			}
		} catch (IOException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
	}
}
