package com.java.peldafeladat.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.peldafeladat.dto.OkmanyDTO;
import com.java.peldafeladat.dto.SzemelyDTO;
import com.java.peldafeladat.model.Allampolgarsag;
import com.java.peldafeladat.model.ErrorListWithOkmanyWrapper;
import com.java.peldafeladat.model.ErrorListWithSzemelyWrapper;
import com.java.peldafeladat.service.SzemelyService;
import com.java.peldafeladat.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

@Service
public class SzemelyServiceImpl implements SzemelyService {
	
	@Value("${app.baseUrl}")
	String baseUrl;
	
	@Autowired
	RestTemplate restTemplate;
	
	private List<Allampolgarsag> allampolgarsagDTOList;
	
	@PostConstruct
	public void loadAllampolgarsagDTOList() {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<Allampolgarsag>> typeReference = new TypeReference<List<Allampolgarsag>>(){};
		try (InputStream inputStream = TypeReference.class.getResourceAsStream("/json/kodszotar21_allampolg.json")) {
			allampolgarsagDTOList = mapper.readValue(inputStream, typeReference);
		} catch (IOException e) {
			System.out.println(ExceptionUtils.getStackTrace(e));
		}
	}

    @Override
    public String validateSzemely(SzemelyDTO szemelyDTO) {
    	List<String> errorList = new ArrayList<>();
    	if (szemelyDTO == null) {
    		errorList.add("Nem adott meg személy objektumot!");
    		return Utils.convertObjectToJsonString(new ErrorListWithSzemelyWrapper(szemelyDTO, errorList));
    	}
    	
    	SzemelyDTO newSzemelyDTO = new SzemelyDTO();
    	newSzemelyDTO.setVisNev(szemelyDTO.getVisNev());
    	newSzemelyDTO.setSzulNev(szemelyDTO.getSzulNev());
    	newSzemelyDTO.setaNev(szemelyDTO.getaNev());
    	newSzemelyDTO.setSzulDat(szemelyDTO.getSzulDat());
    	newSzemelyDTO.setNeme(szemelyDTO.getNeme());
    	newSzemelyDTO.setAllampKod(szemelyDTO.getAllampKod());
    	
    	// Viselt név
    	validateNameField("Viselt neve", szemelyDTO.getVisNev(), errorList);
    	
    	// Születési név
    	validateNameField("Születési neve", szemelyDTO.getSzulNev(), errorList);
    	
    	// Anyja neve
    	validateNameField("Anyja neve", szemelyDTO.getaNev(), errorList);
    	
    	// Születési idő
    	if (szemelyDTO.getSzulDat() == null) {
    		errorList.add("Születési idő megadása kötelező!");
    	} else {
    		LocalDate szulDat = Utils.convertDateToLocalDate(szemelyDTO.getSzulDat());
    		LocalDate before18YearsFromNow = LocalDate.now().minusYears(18);
    		LocalDate before120YearsFromNow = LocalDate.now().minusYears(120);
    		
    		if (szulDat.isBefore(before120YearsFromNow)) {
    			errorList.add("A személy legfeljebb 120 éves lehet!");
    		}
    		
    		if (szulDat.isAfter(before18YearsFromNow)) {
    			errorList.add("A személynek legalább 18 évesnek kell lennie!");
    		}
    	}
    	
    	// Neme
    	if (StringUtils.isBlank(szemelyDTO.getNeme())) {
    		errorList.add("Nem megadása kötelező!");
    	} else {
    		if (!szemelyDTO.getNeme().equals("F") && !szemelyDTO.getNeme().equals("N")) {
    			errorList.add("Nem megfelelő értéket adott meg a nem mezőnek! Lehetséges értékek: F, N");
    		}
    	}
    	
    	// Állampolgárság
    	if (StringUtils.isBlank(szemelyDTO.getAllampKod())) {
    		errorList.add("Állampolgárság megadása kötelező!");
    	} else {
    		Optional<Allampolgarsag> allampolgarsagOptional = allampolgarsagDTOList.stream().filter(item -> item.getKod().equals(szemelyDTO.getAllampKod())).findFirst();
    		if (allampolgarsagOptional.isPresent()) {
    			Allampolgarsag allampolgarsag = allampolgarsagOptional.get();
    			newSzemelyDTO.setAllampDekod(allampolgarsag.getAllampolgarsag());
    		} else {
    			errorList.add("Nem létező állampolgárságot adott meg!");
    		}
    	}
    	
    	if (szemelyDTO.getOkmLista() != null && !szemelyDTO.getOkmLista().isEmpty()) {
    		ErrorListWithOkmanyWrapper[] errorListWithOkmanyWrapperArray = restTemplate.postForObject(baseUrl + "okmany/", szemelyDTO.getOkmLista(), ErrorListWithOkmanyWrapper[].class);
    		List<ErrorListWithOkmanyWrapper> errorListWithOkmanyWrapperList = Arrays.asList(errorListWithOkmanyWrapperArray);
    		List<OkmanyDTO> okmanyDTOList = new ArrayList<>();
    		for (ErrorListWithOkmanyWrapper errorListWithOkmanyWrapper : errorListWithOkmanyWrapperList) {
    			errorList.addAll(errorListWithOkmanyWrapper.getErrorList());
    			if (errorListWithOkmanyWrapper.getOkmanyDTO() != null) {
    				okmanyDTOList.add(errorListWithOkmanyWrapper.getOkmanyDTO());
    			}
    		}
    		
    		newSzemelyDTO.setOkmLista(okmanyDTOList);
    	}
    	
    	return Utils.convertObjectToJsonString(new ErrorListWithSzemelyWrapper(newSzemelyDTO, errorList));
    }
    
    private void validateNameField(String fieldName, String fieldValue, List<String> errorList) {
    	if (StringUtils.isBlank(fieldValue)) {
    		errorList.add(fieldName + " mező megadása kötelező!");
    		return;
    	}
    	
    	if (fieldValue.length() > 80) {
    		errorList.add(fieldName + " mező nem lehet hosszabb 80 karakternél!");
    	}
    	
    	List<String> nameParts = Arrays.asList(fieldValue.split(" "));
    	if (nameParts.isEmpty()) {
    		errorList.add(fieldName + " mezőnek legalább két részből kell állnia!");
    	} else {
    		if (nameParts.contains("Dr.") && nameParts.size() <= 2) {
    			errorList.add(fieldName + " mezőnek legalább két részből kell állnia a Dr.-on kívül!");
    		}
    		
    		boolean isNamePatternMatches = Pattern.matches("[\\sA-zäÄ\\.\\/\\'\\-\\á\\é\\í\\ó\\ö\\ő\\ú\\ü\\ű]*", fieldValue);
    		if (!isNamePatternMatches) {
    			errorList.add(fieldName + " mező csak az alábbi karaktereket tartalmazhatja: magyar ABC, Ä, pont, perjel, aposztróf, kötőjel és szóköz!");
    		}
    	}
    }
}
