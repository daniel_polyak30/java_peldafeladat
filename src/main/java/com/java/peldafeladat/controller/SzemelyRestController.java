package com.java.peldafeladat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.java.peldafeladat.dto.SzemelyDTO;
import com.java.peldafeladat.service.SzemelyService;

@RestController
@RequestMapping("/szemely")
public class SzemelyRestController {

    @Autowired
    private SzemelyService szemelyService;

    @PostMapping(value = "/")
    public String validateSzemely(@RequestBody SzemelyDTO szemelyDTO) {
    	return szemelyService.validateSzemely(szemelyDTO);
    }
}
