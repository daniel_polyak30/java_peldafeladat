package com.java.peldafeladat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.peldafeladat.dto.OkmanyDTO;
import com.java.peldafeladat.model.ErrorListWithOkmanyWrapper;
import com.java.peldafeladat.service.OkmanyService;

@RestController
@RequestMapping("/okmany")
public class OkmanyRestController {

	@Autowired
    private OkmanyService okmanyService;

    @PostMapping(value = "/")
    public List<ErrorListWithOkmanyWrapper> validateOkmanyList(@RequestBody List<OkmanyDTO> okmanyDTOs) {
    	return okmanyService.validateOkmanyDTOList(okmanyDTOs);
    }
}
